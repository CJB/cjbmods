package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class CJB_PlayerControllerSP extends PlayerControllerSP
{
	
    public CJB_PlayerControllerSP(Minecraft minecraft)
    {
        super(minecraft);
    }

    public float getBlockReachDistance()
    {
        return CJB.modcheats && CJB.extendedreach ? 2000F : super.getBlockReachDistance();
    }
    
    public boolean onPlayerDestroyBlock(int i, int j, int k, int l)
    {
        Block block = Block.blocksList[mc.theWorld.getBlockId(i, j, k)];
        
        if (block == null) return false;
        
        if (block != null &&block == Block.mobSpawner && CJB.mobspawner)
        {
        	mod_cjb_cheats.dropBlockAsItem(mc.theWorld, i, j, k, new ItemStack(Block.mobSpawner));
        }
        return super.onPlayerDestroyBlock(i, j, k, l);
    }
    
    public boolean extendedReach()
    {
        return CJB.modcheats && CJB.extendedreach;
    }
    
    public boolean onPlayerRightClick(EntityPlayer entityplayer, World world, ItemStack itemstack, int i, int j, int k, int l)
    {
        int i1 = world.getBlockId(i, j, k);
        if (CJB.modcheats && CJB.mobspawner) {
			if (i1 == Block.mobSpawner.blockID){
				TileEntityMobSpawner tile = (TileEntityMobSpawner) world.getBlockTileEntity(i, j, k);
				
				if (tile != null) {
					mc.displayGuiScreen(new CJB_GuiMobSpawner(tile));
					return true;
				}
			}
		}
        
        if (!CJB.modcheats || !CJB.infiniteitems)
        	return super.onPlayerRightClick(entityplayer, world, itemstack, i, j, k, l);
        
        if (i1 > 0 && Block.blocksList[i1].blockActivated(world, i, j, k, entityplayer))
            return true;
		
        if (itemstack == null)
            return false;
        else
        {
            int j1 = itemstack.getItemDamage();
            int k1 = itemstack.stackSize;
            boolean flag = itemstack.useItem(entityplayer, world, i, j, k, l);
            itemstack.setItemDamage(j1);
            itemstack.stackSize = k1;
            return flag;
        }
    }
}

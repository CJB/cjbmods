package net.minecraft.src;

import java.io.File;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;

public class mod_cjb_items extends BaseMod {
	
	Minecraft m = null;
	World w = null;
	public static boolean hasGoggles = false;
	
	public static Item WoN;
	public static Item WoNe;
	public static Item Goggles;
	public static Item PWB;
	public static Item RW;
	public static Item MS;
	public static Item PF;
	public static Item Sack;
	public static Item mobplacer;
	
	public static Block pressurePlatePlankRev;
	public static Block pressurePlateStoneRev;
	public static Block pressurePlateIron;
	public static Block pressurePlateGold;
	public static Block pressurePlateIronRev;
	public static Block pressurePlateGoldRev;
	
	public static CJB_BlockTrashcan Trashcan;
	public static CJB_RecyclerBlock Recycler;
	public static CJB_BlockMesh Mesh;
	
	public static boolean gogglesEquiped;
	
	public static boolean wonb = CJB_Settings.getBoolean("item.WrathofNotch", true);
	public static boolean gogglesb = CJB_Settings.getBoolean("item.Goggles", true);
	public static boolean pwbb = CJB_Settings.getBoolean("item.PortableWorkbench", true);
	public static boolean rwb = CJB_Settings.getBoolean("item.RazorWind", true);
	public static boolean msb = CJB_Settings.getBoolean("item.MobSpawner", true);
	public static boolean pfb = CJB_Settings.getBoolean("item.PortableFurnace", true);
	public static boolean sackb = CJB_Settings.getBoolean("item.Sack", true);
	public static boolean mobplacerb = CJB_Settings.getBoolean("item.MobPlacer", true);
	
	public static int pppr = CJB_Settings.getInteger("block.PressurePlatePlanksReversedID", 207);
	public static int ppsr = CJB_Settings.getInteger("block.PressurePlateStoneReversedID", 208);
	public static int ppi = CJB_Settings.getInteger("block.PressurePlateIronID", 200);
	public static int ppg = CJB_Settings.getInteger("block.PressurePlateGoldID", 201);
	public static int ppir = CJB_Settings.getInteger("block.PressurePlateIronReversedID", 202);
	public static int ppgr = CJB_Settings.getInteger("block.PressurePlateGoldReversedID", 203);
	public static int trash = CJB_Settings.getInteger("block.TrashcanID", 204);
	public static int recyc = CJB_Settings.getInteger("block.RecyclerID", 205);
	public static int mesh = CJB_Settings.getInteger("block.MeshID", 206);
	
	public static int TrashcanModelID;
	public static int MeshModelID;
	public static int recyclertex1;
	public static int recyclertex2;
	public static int trashtex1;
	public static int trashtex2;
	
	public static int sacktex1;
	public static int sacktex2;
	public static int sacktex3;
	
	@SuppressWarnings("unchecked")
	public void AddRenderer(@SuppressWarnings("rawtypes") Map map)
    {
		map.put(net.minecraft.src.CJB_ForceEntity.class, new CJB_ForceRender());
    }
	
	public mod_cjb_items() 
	{	
		if (wonb){
			WoN = (new CJB_ItemWoN(3920-256, 9)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/items/won.png")).setItemName("WoN");
			WoNe = (new CJB_ItemWoN(3921-256, 0)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/items/wone.png")).setItemName("WoNe");
			ModLoader.addName(WoN, "Wrath of Notch");
			ModLoader.addName(WoNe, "Wrath of Notch");
			ModLoader.addRecipe(new ItemStack(WoN, 1), new Object[]{"#", "X", "X", Character.valueOf('X'), Block.blockDiamond, Character.valueOf('#'), Block.glowStone });
			ModLoader.addRecipe(new ItemStack(WoN, 1), new Object[]{"#", "X", Character.valueOf('X'), WoNe, Character.valueOf('#'), Block.glowStone });
		}
		
		if (gogglesb) {
			Goggles = (new ItemArmor(3922-256, EnumArmorMaterial.CLOTH, ModLoader.addArmor("goggles"), 0)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/items/goggles.png")).setItemName("itemGoggles");
			ModLoader.addName(Goggles, "Nightvision Goggles");
			ModLoader.addRecipe(new ItemStack(Goggles, 1), new Object[]{"L L","L#L", "#X#", Character.valueOf('L'), Item.leather, Character.valueOf('X'), Item.ingotIron, Character.valueOf('#'), Block.glowStone });
		}
		
		if (pwbb) {
			PWB = (new CJB_PWBItem(3923-256, 0)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/items/pwb.png")).setItemName("PWB");
			ModLoader.addName(PWB, "Portable Workbench");
			ModLoader.addRecipe(new ItemStack(PWB, 1), new Object[]{"##","##", Character.valueOf('#'), Item.stick});
		}
		
		if (rwb) {
			RW = (new CJB_ItemRazorWind(3924-256)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/items/razorwing.png")).setItemName("razorwind");
			ModLoader.addName(RW, "Razor Wind");
			ModLoader.addRecipe(new ItemStack(RW, 1), new Object[]{"LPP","LPP","LLL", Character.valueOf('L'), Item.stick, Character.valueOf('P'), Item.paper});
			ModLoader.registerEntityID(net.minecraft.src.CJB_ForceEntity.class, "Force", ModLoader.getUniqueEntityId());
		}
		
		if (msb){
			MS = (new CJB_ItemMobSpawner(3925-256)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/items/mobspawner.png")).setItemName("MobSpawner");
			ModLoader.addName(MS, "Mobster Wand");
			ModLoader.addRecipe(new ItemStack(MS, 1), new Object[]{" L "," P "," P ", Character.valueOf('P'), Item.blazeRod, Character.valueOf('L'), Item.eyeOfEnder});
		}
		
		if (pfb) {
			PF = (new CJB_PFItem(3926-256)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/items/portablefurnace.png")).setItemName("PF");
			ModLoader.addName(PF, "Portable Furnace");
			ModLoader.addRecipe(new ItemStack(PF, 1), new Object[]{"###","#X#", "# #", Character.valueOf('#'), Block.cobblestone, Character.valueOf('X'), Item.bucketLava});
		}
		
		if (sackb) {
			Sack = (new CJB_SackItem(3927-256)).setItemName("Sack");
			ModLoader.addRecipe(new ItemStack(Sack, 1, 0), new Object[]{" # ","XXX", "X$X", Character.valueOf('#'), Item.silk, Character.valueOf('X'), Item.leather, Character.valueOf('$'), Item.ingotIron});
			ModLoader.addRecipe(new ItemStack(Sack, 1, 1), new Object[]{" # ","XXX", "X$X", Character.valueOf('#'), Item.silk, Character.valueOf('X'), Item.leather, Character.valueOf('$'), Item.ingotGold});
			ModLoader.addRecipe(new ItemStack(Sack, 1, 2), new Object[]{" # ","XXX", "X$X", Character.valueOf('#'), Item.silk, Character.valueOf('X'), Item.leather, Character.valueOf('$'), Item.diamond});
			
			sacktex1 = ModLoader.addOverride("/gui/items.png", "/cjb/items/sack-iron.png");
			sacktex2 = ModLoader.addOverride("/gui/items.png", "/cjb/items/sack-gold.png");
			sacktex3 = ModLoader.addOverride("/gui/items.png", "/cjb/items/sack-diamond.png");
		}
		
		if (mobplacerb) {
			mobplacer = (new CJB_ItemMonsterPlacer(3928-256)).setIconCoord(9, 9).setItemName("monsterPlacer");
		}

		if (pppr > 0) {
			pressurePlatePlankRev = (new CJB_BlockPressurePlate(pppr, Block.planks.blockIndexInTexture, "everything", Material.wood, true)).setHardness(0.5F).setStepSound(Block.soundWoodFootstep).setBlockName("pressurePlatePlanksRev").setRequiresSelfNotify();
			ModLoader.registerBlock(pressurePlatePlankRev);
			ModLoader.addName(pressurePlatePlankRev, "Pressure Plate Plank Reversed");
			ModLoader.addRecipe(new ItemStack(pressurePlatePlankRev, 1), new Object[]{"#", "$", Character.valueOf('#'), Block.pressurePlatePlanks, Character.valueOf('$'), Block.torchRedstoneActive});
		}
		
		if (ppsr > 0) {
			pressurePlateStoneRev = (new CJB_BlockPressurePlate(ppsr, Block.stone.blockIndexInTexture, "living", Material.rock, true)).setHardness(0.5F).setStepSound(Block.soundStoneFootstep).setBlockName("pressurePlateStoneRev").setRequiresSelfNotify();
			ModLoader.registerBlock(pressurePlateStoneRev);
			ModLoader.addName(pressurePlateStoneRev, "Pressure Plate Stone Reversed");
			ModLoader.addRecipe(new ItemStack(pressurePlateStoneRev, 1), new Object[]{"#", "$", Character.valueOf('#'), Block.pressurePlateStone, Character.valueOf('$'), Block.torchRedstoneActive});
		}
		
		if (ppi > 0) {
			pressurePlateIron = (new CJB_BlockPressurePlate(ppi, Block.blockSteel.blockIndexInTexture, "players", Material.iron, false)).setHardness(0.5F).setStepSound(Block.soundMetalFootstep).setBlockName("pressurePlateIron").setRequiresSelfNotify();
			ModLoader.registerBlock(pressurePlateIron);
			ModLoader.addName(pressurePlateIron, "Pressure Plate Iron");
			ModLoader.addRecipe(new ItemStack(pressurePlateIron, 1), new Object[]{"##", Character.valueOf('#'), Item.ingotIron});
		}
		
		if (ppg > 0) {
			pressurePlateGold = (new CJB_BlockPressurePlate(ppg, Block.blockGold.blockIndexInTexture, "creatures", Material.iron, false)).setHardness(0.5F).setStepSound(Block.soundMetalFootstep).setBlockName("pressurePlateGold").setRequiresSelfNotify();
			ModLoader.registerBlock(pressurePlateGold);
			ModLoader.addName(pressurePlateGold, "Pressure Plate Gold");
			ModLoader.addRecipe(new ItemStack(pressurePlateGold, 1), new Object[]{"##", Character.valueOf('#'), Item.ingotGold});
		}
		
		if (ppir > 0) {
			pressurePlateIronRev = (new CJB_BlockPressurePlate(ppir, Block.blockSteel.blockIndexInTexture, "players", Material.iron, true)).setHardness(0.5F).setStepSound(Block.soundMetalFootstep).setBlockName("pressurePlateIronRev").setRequiresSelfNotify();
			ModLoader.registerBlock(pressurePlateIronRev);
			ModLoader.addName(pressurePlateIronRev, "Pressure Plate Iron Reversed");
			ModLoader.addRecipe(new ItemStack(pressurePlateIronRev, 1), new Object[]{"#", "$", Character.valueOf('#'), pressurePlateIron, Character.valueOf('$'), Block.torchRedstoneActive});
		}
		
		if (ppgr > 0) {
			pressurePlateGoldRev = (new CJB_BlockPressurePlate(ppgr, Block.blockGold.blockIndexInTexture, "creatures", Material.iron, true)).setHardness(0.5F).setStepSound(Block.soundMetalFootstep).setBlockName("pressurePlateGoldRev").setRequiresSelfNotify();
			ModLoader.registerBlock(pressurePlateGoldRev);
			ModLoader.addName(pressurePlateGoldRev, "Pressure Plate Gold Reversed");
			ModLoader.addRecipe(new ItemStack(pressurePlateGoldRev, 1), new Object[]{"#", "$", Character.valueOf('#'), pressurePlateGold, Character.valueOf('$'), Block.torchRedstoneActive});
		}
		
		if (trash > 0) {
			Trashcan = (CJB_BlockTrashcan)new CJB_BlockTrashcan(trash).setHardness(2.5F).setStepSound(Block.soundWoodFootstep).setBlockName("trashcan").setRequiresSelfNotify();
			ModLoader.registerBlock(Trashcan);
			ModLoader.addName(Trashcan, "Trashcan");
			ModLoader.addRecipe(new ItemStack(Trashcan, 1), new Object[]{"# #", "# #", "###", Character.valueOf('#'), Block.wood});
			trashtex1 = ModLoader.addOverride("/terrain.png", "/cjb/trashcan1.png");
			trashtex2 = ModLoader.addOverride("/terrain.png", "/cjb/trashcan2.png");
			TrashcanModelID = ModLoader.getUniqueBlockModelID(this, true);
		}
		
		if (recyc > 0) {
			Recycler = (CJB_RecyclerBlock)new CJB_RecyclerBlock(recyc).setHardness(3.5F).setStepSound(Block.soundStoneFootstep).setBlockName("recycler").setRequiresSelfNotify();
			ModLoader.registerBlock(Recycler);
			ModLoader.addName(Recycler, "Recycler");
			ModLoader.addRecipe(new ItemStack(Recycler, 1), new Object[]{" # ", "#$#", " # ", Character.valueOf('#'), Block.blockLapis, Character.valueOf('$'), Item.bow});
			ModLoader.registerTileEntity(net.minecraft.src.CJB_RecyclerTileEntity.class, "Recycler");
			recyclertex1 = ModLoader.addOverride("/terrain.png", "/cjb/recycler1.png");
			recyclertex2 = ModLoader.addOverride("/terrain.png", "/cjb/recycler2.png");
		}
		
		if (mesh > 0) {
			Mesh = (CJB_BlockMesh) new CJB_BlockMesh(mesh, Material.wood).setHardness(3F).setStepSound(Block.soundWoodFootstep).setBlockName("mesh").disableStats().setRequiresSelfNotify();
			ModLoader.registerBlock(Mesh);
			ModLoader.addName(Mesh, "Mesh");
			ModLoader.addRecipe(new ItemStack(Mesh, 16), new Object[]{"# #", " # ", "# #", Character.valueOf('#'), Item.ingotIron});
			MeshModelID = ModLoader.getUniqueBlockModelID(this, true);
		}

		ModLoader.setInGameHook(this, true, false);

		if (gogglesb)
			Goggles.setMaxDamage(0);
		
		File file = new File(Minecraft.getMinecraftDir(), "resources/newsound/cjb/goggles.ogg");
		ModLoader.getMinecraftInstance().installResource("newsound/cjb/goggles.ogg", file);
		file = new File(Minecraft.getMinecraftDir(), "resources/newsound/cjb/swoosh.ogg");
		ModLoader.getMinecraftInstance().installResource("newsound/cjb/swoosh.ogg", file);
	}
	
	public boolean onTickInGame(float f, Minecraft mc)
    {
		CJB.moditems = true;	
		renderMod(mc);
		return true;
    }
	
	@Override
	public void addRenderer(@SuppressWarnings("rawtypes") Map map)
    {
		map.put(net.minecraft.src.CJB_EntityEgg.class, new RenderSnowball(Item.egg.getIconFromDamage(0)));
    }
	
	public void renderMod(Minecraft mc)
	{
		m = mc;
		
		if (mobplacerb && !(Item.egg instanceof CJB_ItemEgg) ) {
			Item.itemsList[Item.egg.shiftedIndex] = null;
			Item.egg = (new CJB_ItemEgg(88)).setIconCoord(12, 0).setItemName("egg");
		}
		
    	if(m.theWorld == null || m.theWorld != w){
    		w = m.theWorld;
    		hasGoggles = false;
    		return;
    	}
    	
		EntityPlayer plr = m.thePlayer != null? m.thePlayer : null;
		if (gogglesb && plr != null)
		{
			hasGoggles = false;
			for(ItemStack itemstack : plr.inventory.armorInventory)
			{
				if (itemstack != null && itemstack.getItem() == Goggles)
					hasGoggles = true;
			}
			
			if (hasGoggles && !gogglesEquiped) {
				gogglesEquiped = true;
				w.playSoundAtEntity(m.thePlayer, "cjb.goggles", 1F, 1F);
			}
			if (!hasGoggles && gogglesEquiped) {
				gogglesEquiped = false;
			}
			
			if (hasGoggles)
			{
				GL11.glPushMatrix();
				ScaledResolution scaledresolution = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
		        int i = scaledresolution.getScaledWidth();
		        int j = scaledresolution.getScaledHeight();
		        GL11.glDisable(2896 /*GL_LIGHTING*/);
		        GL11.glDisable(2929 /*GL_DEPTH_TEST*/);
		        m.ingameGUI.drawRect(0, 0, i, j, 0x3300FF00);
		        GL11.glPopMatrix();
			}
		}
	}
    
    public boolean renderWorldBlock(RenderBlocks renderblocks, IBlockAccess iblockaccess, int i, int j, int k, Block block, int l)
    {
        if(l == TrashcanModelID)
        {
            return Trashcan.RenderInWorld(renderblocks, iblockaccess, i, j, k, block);
        } else
        if(l == MeshModelID)
        {
            return Mesh.RenderInWorld(renderblocks, iblockaccess, i, j, k, block);
        } else
        {
            return false;
        }
    }
    
    public void renderInvBlock(RenderBlocks renderblocks, Block block, int i, int j)
    {
        if(j == TrashcanModelID)
        {
            Trashcan.RenderInInv(renderblocks, block, i);
        }
        if(j == MeshModelID) 
        {
        	Mesh.RenderInInv(renderblocks, block, i);
        }
    }
    
    public String getVersion() {
		return CJB.VERSION;
	}

	public void load() {
	} 
	
}
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            Slot, ItemStack, ItemArmor, Item, 
//            Block, ContainerPlayer, IInventory

class CJB_DogSlotArmor extends Slot
{

    CJB_DogSlotArmor(CJB_DogContainer containerdog, IInventory iinventory, int i, int j, int k)
    {
        super(iinventory, i, j, k);
        inventory = containerdog;
        armorType = 1;
    }

    public int getSlotStackLimit()
    {
        return 1;
    }

    public boolean isItemValid(ItemStack itemstack)
    {
        if(itemstack.getItem() instanceof ItemArmor)
        {
            return ((ItemArmor)itemstack.getItem()).armorType == armorType;
        }
        return false;
    }

    final int armorType; /* synthetic field */
    final CJB_DogContainer inventory; /* synthetic field */
}

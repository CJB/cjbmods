package net.minecraft.src;

import org.lwjgl.input.Keyboard;

public class CJB_ItemMobSpawner extends Item
{

    public CJB_ItemMobSpawner(int i)
    {
        super(i);
        maxStackSize = 1;
    }
    
    @Override
    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
    	if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)) 
    	{
    		ModLoader.getMinecraftInstance().displayGuiScreen(new CJB_GuiMobSpawner());
    		return itemstack;
    	}
    	
    	
    	MovingObjectPosition mov = ModLoader.getMinecraftInstance().objectMouseOver;
    	
    	if (mov != null && mov.entityHit != null)
    	{
    		if (mov.entityHit instanceof EntityVillager)
    		{
    			EntityVillager ent = (EntityVillager)mov.entityHit;
    			try {
					int i1 = (Integer) ModLoader.getPrivateValue(EntityVillager.class, ent, 0);
					i1 = i1 > 3 ? 0 : ++i1;
					
					if(i1 == 0)
			        {
			            ent.texture = "/mob/villager/farmer.png";
			        }
			        if(i1 == 1)
			        {
			            ent.texture = "/mob/villager/librarian.png";
			        }
			        if(i1 == 2)
			        {
			            ent.texture = "/mob/villager/priest.png";
			        }
			        if(i1 == 3)
			        {
			            ent.texture = "/mob/villager/smith.png";
			        }
			        if(i1 == 4)
			        {
			            ent.texture = "/mob/villager/butcher.png";
			        }
					ModLoader.setPrivateValue(EntityVillager.class, ent, 0, i1);
				} catch (Throwable e) {
					e.printStackTrace();
				}
    		}
    			
    		return itemstack;
    	}
    	
    	return itemstack;
    }
    
    @Override
    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
    	if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)) 
    		return false;
    	
    	i += Facing.offsetsXForSide[l];
        j += Facing.offsetsYForSide[l];
        k += Facing.offsetsZForSide[l];
        Entity entity = (EntityLiving)EntityList.createEntityByName(CJB_GuiMobSpawner.mob, world);
        if (entity != null)
        {
            entity.setLocationAndAngles((double)i + 0.5D, j, (double)k + 0.5D, 0.0F, 0.0F);
            world.spawnEntityInWorld(entity);
        }
    	return true;
    }

    public boolean isFull3D()
    {
        return true;
    }
}

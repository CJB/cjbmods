package net.minecraft.src;

public class CJB_ItemDye extends ItemDye
{

    public CJB_ItemDye(int i)
    {
        super(i);
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(itemstack.getItemDamage() == 15 && !world.isRemote)
        {
            int i1 = world.getBlockId(i, j, k);
            if(i1 == Block.dirt.blockID)
            {
                world.setBlockWithNotify(i, j, k, Block.grass.blockID);
                itemstack.stackSize--;
                return true;
            }
        }
        return super.onItemUse(itemstack, entityplayer, world, i, j, k, l);
    }
}

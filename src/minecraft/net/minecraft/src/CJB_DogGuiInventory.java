package net.minecraft.src;

import org.lwjgl.opengl.GL11;

public class CJB_DogGuiInventory extends GuiContainer
{
	
	private float xSize_lo;
    private float ySize_lo;
    
    private CJB_DogEntity dogentity;
    
	public CJB_DogGuiInventory(InventoryPlayer inventoryplayer, CJB_DogInventory inventorydog, CJB_DogEntity ent)
    {
        super(new CJB_DogContainer(inventoryplayer, inventorydog));
        dogentity = ent;
    }

	@Override
    protected void drawGuiContainerForegroundLayer()
    {
        fontRenderer.drawString("Dog Inventory", 52, 6, 0x404040);
        fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
    }
    
    public void drawScreen(int i, int j, float f)
    {
        super.drawScreen(i, j, f);
        xSize_lo = i;
        ySize_lo = j;
    }
    
    public boolean doesGuiPauseGame()
    {
        return true;
    }

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		int k = mc.renderEngine.getTexture("/cjb/mobs/dog/doginventory.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.renderEngine.bindTexture(k);
        int l = guiLeft;
        int i1 = guiTop;
        drawTexturedModalRect((width - xSize) / 2, (height - ySize) / 2, 0, 0, xSize, ySize);
        GL11.glEnable(32826 /*GL_RESCALE_NORMAL_EXT*/);
        GL11.glEnable(2903 /*GL_COLOR_MATERIAL*/);
        GL11.glPushMatrix();
        GL11.glTranslatef(l + 145, i1 + 70, 20F);
        float f1 = 45F;
        GL11.glScalef(-f1, f1, f1);
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        float f2 = dogentity.renderYawOffset;
        float f3 = dogentity.rotationYaw;
        float f4 = dogentity.rotationPitch;
        float f5 = (float)(l + 145) - xSize_lo;
        float f6 = ((float)((i1 + 90) - 50) - ySize_lo) / 5;
        GL11.glRotatef(135F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glRotatef(-135F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-(float)Math.atan(f6 / 40F) * 20F, 1.0F, 0.0F, 0.0F);
        dogentity.renderYawOffset = (float)Math.atan(f5 / 40F) * 20F;
        dogentity.rotationYaw = (float)Math.atan(f5 / 40F) * 40F;
        dogentity.rotationPitch = -(float)Math.atan(f6 / 40F) * 20F;
        GL11.glTranslatef(0.0F, 0, 0.0F);
        RenderManager.instance.playerViewY = 180;
        RenderManager.instance.playerViewX = 0;
        RenderManager.instance.renderEntityWithPosYaw(dogentity, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
        dogentity.renderYawOffset = f2;
        dogentity.rotationYaw = f3;
        dogentity.rotationPitch = f4;
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(32826 /*GL_RESCALE_NORMAL_EXT*/);
	}
    
}

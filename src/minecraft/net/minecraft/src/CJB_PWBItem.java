package net.minecraft.src;

public class CJB_PWBItem extends Item
{

    public CJB_PWBItem(int i, int j)
    {
        super(i);
        maxStackSize = 1;
        setMaxDamage(j);
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
    	ModLoader.getMinecraftInstance().displayGuiScreen(new CJB_PWBGuiCrafting(entityplayer.inventory, world));
    	return itemstack;
    }

    public boolean isFull3D()
    {
        return true;
    }
}

package net.minecraft.src;

import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class CJB_Player extends PlayerBase{

	private Minecraft mc;
	private World world;
	private long hitWait;
	
	public CJB_Player(PlayerAPI playerapi) {
		super(playerapi);
		mc = ModLoader.getMinecraftInstance();
		world = mc.theWorld;
	}	
	
	public void moveEntity(double d, double d1, double d2)
    {
		if (!CJB.modcheats)
		{
			super.moveEntity(d, d1, d2);
			return;
		}
		CJB.entityplayer = true;
		boolean fly = CJB.flying;
		int keyup = CJB.KeyFlyUp;
		int keydown = CJB.KeyFlyDown;
		
		float steps = player.distanceWalkedModified;
		if (fly)
		{
			mc.gameSettings.keyBindSneak.pressed = false;
			
			d1 = 0d;
			if (!CJB.guiopen)
			{
				if (Keyboard.isKeyDown(keyup)) { d1 += 0.2F; }
				if (Keyboard.isKeyDown(keydown)) { d1 -= 0.2F; }
				
				boolean speedkey = Keyboard.isKeyDown(CJB.KeyFlySpeed);
				
				double mul = (speedkey || CJB.toggleSpeed || CJB.alwaysspeed) ? (1.0F * CJB.flyspeed) : 1D;
				
				float flyVertical = 0F;
				float pitch = Math.abs((0.0050F * (speedkey || CJB.toggleSpeed || CJB.alwaysspeed ? CJB.flyspeed : 1)) * player.rotationPitch);
				
				if (player.moveForward > 0.01F && CJB.mouseControl)
				{
					if (player.rotationPitch > 0)
						flyVertical -= pitch;
					if (player.rotationPitch < 0)
						flyVertical += pitch;
				}
				else if (player.moveForward < -0.01F && CJB.mouseControl)
				{
					if (player.rotationPitch > 0)
						flyVertical += pitch;
					if (player.rotationPitch < 0)
						flyVertical -= pitch;
				}
								
				d *= mul; 
				d1 *= mul;
				d2 *= mul;

				player.fallDistance = 0f;
				player.motionY = 0;
				if (CJB.mouseControl && d1 == 0D) 
					d1 = flyVertical; 
				
				CJB.onGround = false;
			}
			
		} else {
			if (!CJB.guiopen)
			{
				boolean speedkey = Keyboard.isKeyDown(CJB.KeyFlySpeed);
				double mul = (speedkey || CJB.toggleSpeed) ? (1.0F * CJB.runspeed) : 1D;
				d *= mul; 
				d2 *= mul;
			}
		}
		super.moveEntity(d, d1, d2);
		
		if (fly)
		{
			player.fallDistance = 0f;
			player.onGround = true;
			player.distanceWalkedModified = steps;
		} 
		else if (!CJB.onGround && !player.onGround)
		{
			player.fallDistance = 0f;
			player.onGround = true;
		}
		else
		{
			CJB.onGround = true;
		}
    }
	
	public boolean canPlayerEdit(int i, int j, int k) {
		if (Mouse.isButtonDown(1) || player.capabilities.isCreativeMode)
			return super.canPlayerEdit(i, j, k);
		
		world = ModLoader.getMinecraftInstance().theWorld;
		
		if (CJB.onehitblock && world.getBlockId(i, j, k) != 0 && System.currentTimeMillis() - hitWait > 200) {
			
			hitWait = System.currentTimeMillis();
			
			if (Block.blocksList[world.getBlockId(i, j, k)].blockResistance >= 10000F)
				return super.canPlayerEdit(i, j, k);
			
			mc.playerController.onPlayerDestroyBlock(i, j, k, mc.objectMouseOver.sideHit);
		}
				
		return super.canPlayerEdit(i, j, k);
	}
	
	public boolean canHarvestBlock(Block block) {
		
		if (block == null)
			return false;
		
		if (CJB.nopickaxe){
			return true;
		}
		
		return super.canHarvestBlock(block);
	}
	
	public static void dropBlockAsItem(World world, int i, int j, int k, ItemStack itemstack)
    {
        if(world.isRemote)
        {
            return;
        } else
        {
            float f = 0.7F;
            double d = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            double d1 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            double d2 = (double)(world.rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
            EntityItem entityitem = new EntityItem(world, (double)i + d, (double)j + d1, (double)k + d2, itemstack);
            entityitem.delayBeforeCanPickup = 10;
            world.spawnEntityInWorld(entityitem);
            return;
        }
    }
	
	public boolean canBreatheUnderwater()
	{
		if (CJB.infinitebreath)
			return true;
		
		return super.canBreatheUnderwater();
	}
	
	public boolean attackEntityFrom(DamageSource damagesource, int i)
    {
		if (CJB.nofalldamage && damagesource.damageType == DamageSource.fall.damageType)
			return false;
		
        if (CJB.infinitehealth)
            return false;
 
        return super.attackEntityFrom(damagesource, i);
    }
	
	public void attackTargetEntityWithCurrentItem(Entity ent)
    {
		boolean flag = (ent instanceof EntityTameable && ((EntityTameable)ent).isTamed());
		
		if (CJB.onehitkill && !flag)
			ent.attackEntityFrom(DamageSource.causePlayerDamage(player), 9999);
		
		super.attackTargetEntityWithCurrentItem(ent);
    }
	
	public void onLivingUpdate(){
		if (CJB.infinitexp) {
			player.experienceLevel = 999;
			player.experience = 1f;
		}
		
		if (CJB.autoJump) {
			player.stepHeight = 1.5f;
		} else
			player.stepHeight = 0.5f;
		
		for (ItemStack itemstack : player.inventory.mainInventory)
		{
			if (itemstack == null)
				continue;
			
			if (CJB.unbreakableitems && !(itemstack.getItem() instanceof ItemArmor) && itemstack.isItemStackDamageable()){
					itemstack.setItemDamage(0);
			}

			if (CJB.infinitearrows && itemstack.itemID == Item.arrow.shiftedIndex){
				itemstack.stackSize = 9;
			}
			
			if (CJB.unbreakablearmor && itemstack.getItem() instanceof ItemArmor){
				itemstack.setItemDamage(0);
			}
		}
		
		for (ItemStack itemstack : player.inventory.armorInventory)
		{
			if (itemstack == null)
				continue;
			
			if (CJB.unbreakablearmor && itemstack.getItem() instanceof ItemArmor){
				itemstack.setItemDamage(0);
			}
		}		
		super.onLivingUpdate();
	}
	
	public boolean pushOutOfBlocks(double d, double d1, double d2)
	{
		if (player.noClip)
			return false;
		else
			return super.pushOutOfBlocks(d, d1, d2);
	}
	
	public int getBrightnessForRender(float f)
    {
		if (player.posY < 32D && CJB.blackfog)
			return 0xffffff;
		
		return super.getBrightnessForRender(f);
    }

	public boolean isEntityInsideOpaqueBlock()
    {
        return !player.noClip && !player.sleeping && super.isEntityInsideOpaqueBlock();
    }
}

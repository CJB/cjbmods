// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.src;

import java.util.Random;

// Referenced classes of package net.minecraft.src:
//            BlockContainer, Material, Block, World, 
//            IBlockAccess, TileEntityDispenser, EntityPlayer, ItemStack, 
//            ModLoader, Item, EntityArrow, EntityEgg, 
//            EntitySnowball, EntityItem, Entity, EntityLiving, 
//            MathHelper, TileEntity

public class CJB_RecyclerBlock extends BlockContainer
{

    protected CJB_RecyclerBlock(int i)
    {
        super(i, Material.rock);
        random = new Random();
        blockIndexInTexture = 45;
    }

    public void onBlockAdded(World world, int i, int j, int k)
    {
        super.onBlockAdded(world, i, j, k);
        setDispenserDefaultDirection(world, i, j, k);
    }

    private void setDispenserDefaultDirection(World world, int i, int j, int k)
    {
        int l = world.getBlockId(i, j, k - 1);
        int i1 = world.getBlockId(i, j, k + 1);
        int j1 = world.getBlockId(i - 1, j, k);
        int k1 = world.getBlockId(i + 1, j, k);
        byte byte0 = 3;
        if(Block.opaqueCubeLookup[l] && !Block.opaqueCubeLookup[i1])
        {
            byte0 = 3;
        }
        if(Block.opaqueCubeLookup[i1] && !Block.opaqueCubeLookup[l])
        {
            byte0 = 2;
        }
        if(Block.opaqueCubeLookup[j1] && !Block.opaqueCubeLookup[k1])
        {
            byte0 = 5;
        }
        if(Block.opaqueCubeLookup[k1] && !Block.opaqueCubeLookup[j1])
        {
            byte0 = 4;
        }
        world.setBlockMetadataWithNotify(i, j, k, byte0);
    }

    public int getBlockTexture(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        int i1 = iblockaccess.getBlockMetadata(i, j, k);
        if(l != i1)
        {
            return mod_cjb_items.recyclertex1;
        } else
        {
            return mod_cjb_items.recyclertex2;
        }
    }

    public int getBlockTextureFromSide(int i)
    {
        if(i == 3)
        {
            return mod_cjb_items.recyclertex2;
        } else
        {
            return mod_cjb_items.recyclertex1;
        }
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {

    	CJB_RecyclerTileEntity tileentityrecycler = (CJB_RecyclerTileEntity)world.getBlockTileEntity(i, j, k);
        ModLoader.openGUI(entityplayer, new CJB_RecyclerGui(entityplayer.inventory, tileentityrecycler));
        return true;
    }

    public TileEntity getBlockEntity()
    {
        return new CJB_RecyclerTileEntity();
    }

    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        int l = MathHelper.floor_double((double)((entityliving.rotationYaw * 4F) / 360F) + 0.5D) & 3;
        if(l == 0)
        {
            world.setBlockMetadataWithNotify(i, j, k, 2);
        }
        if(l == 1)
        {
            world.setBlockMetadataWithNotify(i, j, k, 5);
        }
        if(l == 2)
        {
            world.setBlockMetadataWithNotify(i, j, k, 3);
        }
        if(l == 3)
        {
            world.setBlockMetadataWithNotify(i, j, k, 4);
        }
    }

    public void onBlockRemoval(World world, int i, int j, int k)
    {
    	CJB_RecyclerTileEntity tileentityrecycler = (CJB_RecyclerTileEntity)world.getBlockTileEntity(i, j, k);
        for(int l = 0; l < tileentityrecycler.getSizeInventory(); l++)
        {
            ItemStack itemstack = tileentityrecycler.getStackInSlot(l);
            if(itemstack != null)
            {
                float f = random.nextFloat() * 0.8F + 0.1F;
                float f1 = random.nextFloat() * 0.8F + 0.1F;
                float f2 = random.nextFloat() * 0.8F + 0.1F;
                while(itemstack.stackSize > 0) 
                {
                    int i1 = random.nextInt(21) + 10;
                    if(i1 > itemstack.stackSize)
                    {
                        i1 = itemstack.stackSize;
                    }
                    itemstack.stackSize -= i1;
                    EntityItem entityitem = new EntityItem(world, (float)i + f, (float)j + f1, (float)k + f2, new ItemStack(itemstack.itemID, i1, itemstack.getItemDamage()));
                    float f3 = 0.05F;
                    entityitem.motionX = (float)random.nextGaussian() * f3;
                    entityitem.motionY = (float)random.nextGaussian() * f3 + 0.2F;
                    entityitem.motionZ = (float)random.nextGaussian() * f3;
                    world.spawnEntityInWorld(entityitem);
                }
            }
        }

        super.onBlockRemoval(world, i, j, k);
    }

    private Random random;
}

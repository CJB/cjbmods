package net.minecraft.src;

public class CJB_SackItem extends Item
{
    protected CJB_SackItem(int i)
    {
        super(i);
        setMaxStackSize(1);
    }
    
    public int getIconFromDamage(int i)
    {
    	World world = ModLoader.getMinecraftInstance().theWorld;
    	
    	if (world == null) return mod_cjb_items.sacktex1;
    	
    	CJB_SackData sack = getSackData(new ItemStack(this, 1, i), world);
    	if (sack.sackSize == 0)
    		return mod_cjb_items.sacktex1;
    	else if (sack.sackSize == 1)
    		return mod_cjb_items.sacktex2;
    	else if (sack.sackSize == 2)
    		return mod_cjb_items.sacktex3;
    	
        return mod_cjb_items.sacktex1;
    }
    
    public String getItemDisplayName(ItemStack itemstack)
    {
    	World world = ModLoader.getMinecraftInstance().theWorld;
    	
    	if (world == null) return "Iron Sack";
    	
    	CJB_SackData sack = getSackData(itemstack, world);
    	
    	if (sack.sackSize == 0)
    		return "Iron Sack";
    	else if (sack.sackSize == 1)
    		return "Gold Sack";
    	else if (sack.sackSize == 2)
    		return "Diamond Sack";
    	
        return "Iron Sack";
    }
    
    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
    	ModLoader.getMinecraftInstance().displayGuiScreen(new CJB_SackGui(entityplayer.inventory, getSackData(itemstack, world)));
    	return itemstack;
    }

    public CJB_SackData getSackData(ItemStack itemstack, World world)
    {
        String s = "CJB_SACK_" + itemstack.getItemDamage();
        CJB_SackData data = (CJB_SackData)world.loadItemData(net.minecraft.src.CJB_SackData.class, s);
        
        if (data == null)
        {
        	int i = itemstack.getItemDamage();
            itemstack.setItemDamage(world.getUniqueDataId("CJB_SACK"));
            String s1 = "CJB_SACK_" + itemstack.getItemDamage();
            data = new CJB_SackData(s1);
            data.sackSize = i;
            world.setItemData(s1, data);
            data.markDirty();
        } 
        
        return data;
    }

    public void onCreated(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
    	int i = itemstack.getItemDamage();
        itemstack.setItemDamage(world.getUniqueDataId("CJB_SACK"));
        String s = "CJB_SACK_" + itemstack.getItemDamage();
        CJB_SackData data = new CJB_SackData(s);
        data.sackSize = i;
        world.setItemData(s, data);
        data.markDirty();
    }
}

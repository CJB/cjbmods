// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class CJB_PlayerControllerTest extends PlayerControllerCreative
{
	
    public CJB_PlayerControllerTest(Minecraft minecraft)
    {
        super(minecraft);
    }

    public float getBlockReachDistance()
    {
        return CJB.modcheats && CJB.extendedreach ? 2000F : super.getBlockReachDistance();
    }
    
    public boolean onPlayerDestroyBlock(int i, int j, int k, int l)
    {
        Block block = Block.blocksList[mc.theWorld.getBlockId(i, j, k)];
        if (block != null &&block == Block.mobSpawner && CJB.mobspawner)
        {
        	mod_cjb_cheats.dropBlockAsItem(mc.theWorld, i, j, k, new ItemStack(Block.mobSpawner));
        }
        return super.onPlayerDestroyBlock(i, j, k, l);
    }
    
    public boolean onPlayerRightClick(EntityPlayer entityplayer, World world, ItemStack itemstack, int i, int j, int k, int l)
    {
        int i1 = world.getBlockId(i, j, k);
        if (CJB.modcheats && CJB.mobspawner) {
			if (i1 == Block.mobSpawner.blockID){
				TileEntityMobSpawner tile = (TileEntityMobSpawner) world.getBlockTileEntity(i, j, k);
				
				if (tile != null) {
					mc.displayGuiScreen(new CJB_GuiMobSpawner(tile));
					return true;
				}
			}
		}
        return super.onPlayerRightClick(entityplayer, world, itemstack, i, j, k, l);
    }
}

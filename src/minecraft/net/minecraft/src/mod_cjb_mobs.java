package net.minecraft.src;

import java.util.Map;

public class mod_cjb_mobs extends BaseMod {
	
	public static Item dogcollar;
	public static Item dogbiscuit;
	public static boolean dog = CJB_Settings.getBoolean("mobs.dog", true);
	
	public mod_cjb_mobs() 
	{
		CJB.modmobs = true;
		
		if (dog) {
			ModLoader.registerEntityID(net.minecraft.src.CJB_DogEntity.class, "Dog", ModLoader.getUniqueEntityId(), 0x000000, 0x000000);
			ModLoader.registerEntityID(net.minecraft.src.CJB_DogBiscuitEntity.class, "Dog Biscuit", ModLoader.getUniqueEntityId());
			ModLoader.addSpawn(net.minecraft.src.CJB_DogEntity.class, 1, 1, 4, EnumCreatureType.creature, new BiomeGenBase[]{BiomeGenBase.plains});
			dogcollar = (new Item(3940-256)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/mobs/dog/item_dogcollar.png")).setItemName("dogcollar");
			dogbiscuit = (new CJB_DogBiscuitItem(3941-256)).setIconIndex(ModLoader.addOverride("/gui/items.png", "/cjb/mobs/dog/item_dogbiscuit.png")).setItemName("dogbiscuit");
			ModLoader.addName(dogcollar, "Dog Collar");
			ModLoader.addName(dogbiscuit, "Dog Biscuit");
			ModLoader.addRecipe(new ItemStack(dogcollar, 1), new Object[]{"###","#$#", "###", Character.valueOf('#'), Item.leather, Character.valueOf('$'), Item.bone});
			ModLoader.addShapelessRecipe(new ItemStack(dogbiscuit, 8), new Object[]{Item.bread, Item.porkRaw});
		}
	}
	
	@SuppressWarnings("unchecked")
	public void addRenderer(@SuppressWarnings("rawtypes") Map map)
    {
		if(dog) {
			map.put(net.minecraft.src.CJB_DogEntity.class, new CJB_DogRender(new CJB_DogModel(0f), 0F));
			map.put(net.minecraft.src.CJB_DogBiscuitEntity.class, new CJB_DogBiscuitRender(dogbiscuit.getIconFromDamage(0)));
		}
    }
    
	public String getVersion() {
		return CJB.VERSION;
	}

	public void load() {
		
	} 
	
}
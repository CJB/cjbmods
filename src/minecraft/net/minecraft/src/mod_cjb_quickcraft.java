package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class mod_cjb_quickcraft extends BaseMod {
	
	public mod_cjb_quickcraft()
	{
		ModLoader.setInGameHook(this, true, false);
	}

	public boolean onTickInGame(float f, Minecraft mc)
	{
		CJB_Recipe.load();
		
		if (mc.currentScreen == null)
			CJB.quickcraft = true;
		
		if ((!mc.isMultiplayerWorld() || (mc.isMultiplayerWorld() && CJB.pmquickcraft)) && CJB.quickcraft && mc.thePlayer != null && mc.currentScreen != null && mc.currentScreen instanceof GuiCrafting)
			mc.displayGuiScreen(new CJB_GuiCrafting(mc.thePlayer));
		
		return true;
	}
	
	public String getVersion() {
		return CJB.VERSION;
	}

	public void load() {}  
}
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            Container, Slot, TileEntityDispenser, IInventory, 
//            EntityPlayer

public class CJB_DogContainer extends Container
{

    public CJB_DogContainer(IInventory iinventory, IInventory iinventory1)
    {
    	doginventory = iinventory1;
    	
    	for(int j = 0; j < 1; j++)
        {
            addSlot(new CJB_DogSlotArmor(this, doginventory, doginventory.getSizeInventory() - 1 - j, 8, 8 + j * 18));
        }
    	
        for(int i = 0; i < 3; i++)
        {
            for(int l = 0; l < 3; l++)
            {
                addSlot(new Slot(doginventory, l + i * 3, 62 + l * 18, 17 + i * 18));
            }

        }

        for(int j = 0; j < 3; j++)
        {
            for(int i1 = 0; i1 < 9; i1++)
            {
                addSlot(new Slot(iinventory, i1 + j * 9 + 9, 8 + i1 * 18, 84 + j * 18));
            }

        }

        for(int k = 0; k < 9; k++)
        {
            addSlot(new Slot(iinventory, k, 8 + k * 18, 142));
        }

    }

    public boolean isUsableByPlayer(EntityPlayer entityplayer)
    {
        return doginventory.isUseableByPlayer(entityplayer);
    }
    
    public ItemStack getStackInSlot(int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)inventorySlots.get(i);
        if(slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();
            if(i >= 10 && i < 46)
            {
            	mergeItemStack(itemstack1, 1, 10, false);
            } else
            {
            	mergeItemStack(itemstack1, 10, 46, false);
            }
            if(itemstack1.stackSize == 0)
            {
                slot.putStack(null);
            } else
            {
                slot.onSlotChanged();
            }
            if(itemstack1.stackSize != itemstack.stackSize)
            {
                slot.onPickupFromSlot(itemstack1);
            } else
            {
                return null;
            }
        }
        return itemstack;
    }

    private IInventory doginventory;

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return true;
	}
}

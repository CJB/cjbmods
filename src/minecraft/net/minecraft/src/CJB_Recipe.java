package net.minecraft.src;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.client.Minecraft;

public class CJB_Recipe {

	public static List<CJB_Recipe> recipes = new ArrayList<CJB_Recipe>();

	public int id;
	public ItemStack result;
	public ItemStack[] items;
	public HashMap<Integer, Integer> ids = new HashMap();
	public String category;
	public InventoryPlayer inv = new InventoryPlayer(null);

	public static void load() {
		
		if (!recipes.isEmpty() && Math.abs(recipes.size() - CraftingManager.getInstance().getRecipeList().size()) > 3) return;
		
			recipes = new ArrayList<CJB_Recipe>();
			for (int i = 0; i < CraftingManager.getInstance().getRecipeList().size(); i++) {
				try {
					CJB_Recipe rec = new CJB_Recipe();
					rec.id = i;
					IRecipe irecipe = (IRecipe) CraftingManager.getInstance().getRecipeList().get(i);
					rec.result = irecipe.getRecipeOutput();
	
					rec.ids.put(rec.result.itemID, 0);
	
					if (irecipe instanceof ShapedRecipes) {
						rec.items = new ItemStack[9];
						
						int w = (Integer) ModLoader.getPrivateValue(net.minecraft.src.ShapedRecipes.class, (ShapedRecipes)irecipe, 0);
						int h = (Integer) ModLoader.getPrivateValue(net.minecraft.src.ShapedRecipes.class, (ShapedRecipes)irecipe, 1);
						if (w*h > 9) continue;
						
						ItemStack temp[] = (ItemStack[]) ModLoader.getPrivateValue(net.minecraft.src.ShapedRecipes.class, (ShapedRecipes)irecipe, 2);
						for (int j = 0 ; j < temp.length ; j++) {
							int x = j % w;
							int y = j / w;
							rec.items[x + y * 3] = temp[j];
						}
						
						
					} else if (irecipe instanceof ShapelessRecipes) {
						List list = (List) ModLoader.getPrivateValue(net.minecraft.src.ShapelessRecipes.class, (ShapelessRecipes) irecipe, 1);
						rec.items = new ItemStack[list.size()];
						for (int j = 0; j < list.size(); j++) {
							rec.items[j] = (ItemStack) list.get(j);
						}
					}
	
					for (int j = 0; j < rec.items.length; j++) {
						if (rec.items[j] == null)
							continue;
	
						if (!rec.ids.containsKey(rec.items[j].itemID)) {
							rec.ids.put(rec.items[j].itemID, 0);
						}
					}
	
					recipes.add(rec);
				} catch (Throwable e) {
					continue;
				}
			}
	}
	
	public void setFakeInventory(EntityPlayer player) {
		inv = new InventoryPlayer(player);
		inv.copyInventory(player.inventory);
	}
	
	public void setRealInventory(EntityPlayer player) {
		inv = player.inventory;
	}
	
	public boolean isCraftable(EntityPlayer player, boolean flag)
	{
		if (flag) {
			inv = new InventoryPlayer(player);
			inv.copyInventory(player.inventory);
		}
		for (ItemStack item : items)
		{
			if (item == null) continue;
			if (!consumeItem(item)) {
				return false; 
			} else {
				
				if (item.getItem().hasContainerItem()) {
					if (!inv.addItemStackToInventory(new ItemStack(item.getItem().getContainerItem(), 1, 0))) return false;
				}
			}		
		}
		
		if (!inv.addItemStackToInventory(new ItemStack(result.getItem(), result.stackSize, result.getItemDamageForDisplay()))) return false;
		
		return true;
	}
	
	public void craftRecipe(EntityPlayer player) {
		inv = player.inventory;
		for (int i = 0 ; i < items.length ; i++)
		{
			if (items[i] == null) continue;
			if (!consumeItem(items[i])) {
				return; 
			} else {
				
				if (items[i].getItem().hasContainerItem()) {
					inv.addItemStackToInventory(new ItemStack(items[i].getItem().getContainerItem(), 1, 0));
				}
			}
		}
		
		Minecraft mc = ModLoader.getMinecraftInstance();
		
		if (mc.isMultiplayerWorld()){
			Packet250CustomPayload packet = new Packet250CustomPayload();
			packet.channel = "quickcraft";
			packet.length = 4;
			packet.data = new byte[]{(byte) (id >>> 24), (byte) (id >>> 16), (byte) (id >>> 8), (byte) id};
			mc.getSendQueue().addToSendQueue(packet);
		}
		
		ItemStack item = new ItemStack(result.getItem(), result.stackSize, result.getItemDamageForDisplay());
		item.onCrafting(mc.theWorld, player, 0);
		inv.addItemStackToInventory(item);
		
		return;
	}
	
	public boolean containsItem(ItemStack item){
		
		if (item == null) return false;
		
		for (ItemStack is : items)
		{
			if (is == null) continue;
			if (is.itemID != item.itemID) continue;
			
			if (is.getItemDamage() != -1 && is.getItemDamage() != item.getItemDamage()) continue;
			
			return true;
		}
		
		if (result != null) {
			if (item.itemID != result.itemID) return false;
			
			if (result.getItemDamage() != -1 && result.getItemDamage() != item.getItemDamage()) return false;
			
			return true;
		}
		
		return false;
	}
	
	public boolean containsItemResult(ItemStack item){
		if (item != null && result != null) {
			if (item.itemID != result.itemID) return false;
			
			if (result.getItemDamage() != -1 && result.getItemDamage() != item.getItemDamage()) return false;
			
			return true;
		}
		return false;
	}
	
	public CJB_Recipe getRecipeResult(ItemStack item)
	{
		for (CJB_Recipe rec : recipes)
		{
			if (rec.containsItemResult(item))
				return rec;
		}
		
		return null;
	}
	
	public boolean consumeItem(ItemStack is) {
		
		for (int i = 0 ; i < inv.mainInventory.length ; i++)
		{
			if (inv.mainInventory[i] == null || is == null) continue;
			if (is.itemID != inv.mainInventory[i].itemID) continue;
			
			if (is.getItemDamage() != -1 && is.getItemDamage() != inv.mainInventory[i].getItemDamage()) continue;
			
			if (--inv.mainInventory[i].stackSize <= 0)
				inv.mainInventory[i] = null;
			
			CJB.useditems[i] = true;
			return true;
		}
		
		return false;
	}
	
	public int getCategory(Item item) {
		
		if (item == null) return -1;
		
		int i = 1; // Food
		if (item instanceof ItemFood) return i;
		if (item.shiftedIndex == Item.cake.shiftedIndex) return i;
		if (item.shiftedIndex == Block.melon.blockID) return i;
		
		i = 2; // Tools
		if (item instanceof ItemTool || item instanceof ItemHoe) return i;
		if (item.shiftedIndex == Item.shears.shiftedIndex) return i;
		if (item.shiftedIndex == Item.compass.shiftedIndex) return i;
		if (item.shiftedIndex == Item.pocketSundial.shiftedIndex) return i;
		if (item.shiftedIndex == Item.fishingRod.shiftedIndex) return i;
		if (item.shiftedIndex == Item.flintAndSteel.shiftedIndex) return i;
		if (item.shiftedIndex == Item.map.shiftedIndex) return i;
		if (item.shiftedIndex == Item.bed.shiftedIndex) return i;
		if (item.shiftedIndex == Item.sign.shiftedIndex) return i;
		if (item.shiftedIndex == Item.brewingStand.shiftedIndex) return i;
		if (item.shiftedIndex == Item.cauldron.shiftedIndex) return i;
		
		
		if (item.shiftedIndex == Block.torchWood.blockID) return i;
		if (item.shiftedIndex == Block.chest.blockID) return i;
		if (item.shiftedIndex == Block.workbench.blockID) return i;
		if (item.shiftedIndex == Block.ladder.blockID) return i;
		if (item.shiftedIndex == Block.cauldron.blockID) return i;
		if (item.shiftedIndex == Block.enchantmentTable.blockID) return i;
		if (item.shiftedIndex == Block.torchRedstoneActive.blockID) return i;
		if (item.shiftedIndex == Block.stoneOvenIdle.blockID) return i;
		
		
		i = 3; // Weapons
		if (item instanceof ItemSword) return i;
		if (item.shiftedIndex == Item.bow.shiftedIndex) return i;
		if (item.shiftedIndex == Item.arrow.shiftedIndex) return i;
		
		i = 4; // Structure
		if (item.shiftedIndex == Item.doorWood.shiftedIndex) return i;
		if (item.shiftedIndex == Item.doorSteel.shiftedIndex) return i;
		if (item.shiftedIndex == Item.bed.shiftedIndex) return i;
		if (item.shiftedIndex == Block.stairCompactCobblestone.blockID) return i;
		if (item.shiftedIndex == Block.stairCompactPlanks.blockID) return i;
		if (item.shiftedIndex == Block.stairDouble.blockID) return i;
		if (item.shiftedIndex == Block.stairsBrick.blockID) return i;
		if (item.shiftedIndex == Block.stairSingle.blockID) return i;
		if (item.shiftedIndex == Block.stairsNetherBrick.blockID) return i;
		if (item.shiftedIndex == Block.stairsStoneBrickSmooth.blockID) return i;
		if (item.shiftedIndex == Block.stone.blockID) return i;
		if (item.shiftedIndex == Block.stoneBrick.blockID) return i;
		if (item.shiftedIndex == Block.blockGold.blockID) return i;
		if (item.shiftedIndex == Block.blockSteel.blockID) return i;
		if (item.shiftedIndex == Block.blockDiamond.blockID) return i;
		if (item.shiftedIndex == Block.blockLapis.blockID) return i;
		if (item.shiftedIndex == Block.bookShelf.blockID) return i;
		if (item.shiftedIndex == Block.thinGlass.blockID) return i;
		if (item.shiftedIndex == Block.glass.blockID) return i;
		if (item.shiftedIndex == Block.planks.blockID) return i;
		if (item.shiftedIndex == Block.netherBrick.blockID) return i;
		if (item.shiftedIndex == Block.sandStone.blockID) return i;
		if (item.shiftedIndex == Block.blockSnow.blockID) return i;
		if (item.shiftedIndex == Block.blockClay.blockID) return i;
		if (item.shiftedIndex == Block.brick.blockID) return i;
		if (item.shiftedIndex == Block.glowStone.blockID) return i;
		if (item.shiftedIndex == Block.pumpkinLantern.blockID) return i;
		
		
		i = 5; // Armor
		if (item instanceof ItemArmor) return i;
		
		i = 6; // Dye / Wool
		if (item.shiftedIndex == Item.dyePowder.shiftedIndex) return i;
		if (item.shiftedIndex == Block.cloth.blockID) return i;
		
		i = 7; // Vehicles
		if (item instanceof ItemMinecart) return i;
		if (item instanceof ItemBoat) return i;
		
		i = 8; // Mechenics
		if (item.shiftedIndex == Item.redstoneRepeater.shiftedIndex) return i;
		if (item.shiftedIndex == Block.dispenser.blockID) return i;
		if (item.shiftedIndex == Block.pressurePlatePlanks.blockID) return i;
		if (item.shiftedIndex == Block.pressurePlateStone.blockID) return i;
		if (item.shiftedIndex == Block.redstoneLampIdle.blockID) return i;
		if (item.shiftedIndex == Block.rail.blockID) return i;
		if (item.shiftedIndex == Block.railDetector.blockID) return i;
		if (item.shiftedIndex == Block.railPowered.blockID) return i;
		if (item.shiftedIndex == Block.trapdoor.blockID) return i;
		if (item.shiftedIndex == Block.pistonBase.blockID) return i;
		if (item.shiftedIndex == Block.pistonStickyBase.blockID) return i;
		if (item.shiftedIndex == Block.lever.blockID) return i;
		if (item.shiftedIndex == Block.button.blockID) return i;
		if (item.shiftedIndex == Block.music.blockID) return i;
		if (item.shiftedIndex == Block.jukebox.blockID) return i;
		if (item.shiftedIndex == Block.fenceGate.blockID) return i;
		if (item.shiftedIndex == Block.fenceIron.blockID) return i;
		if (item.shiftedIndex == Block.fence.blockID) return i;
		if (item.shiftedIndex == Block.netherFence.blockID) return i;
		if (item.shiftedIndex == Block.tnt.blockID) return i;
		
		return 9;
	}
}
